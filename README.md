Exemple Simplon

## Docs
### Gitlab-ci
https://docs.gitlab.com/ee/ci/yaml/

https://about.gitlab.com/2016/03/01/gitlab-runner-with-docker/
### Gitlab-ci + NodeJS
https://medium.com/@seulkiro/deploy-node-js-app-with-gitlab-ci-cd-214d12bfeeb5
### Chai +  Mocha
http://mherman.org/blog/2015/09/10/testing-node-js-with-mocha-and-chai/

https://buddy.works/guides/how-automate-nodejs-unit-tests-with-mocha-chai

http://www.chaijs.com/api/bdd/
### Systemd + NodeJS
https://blog.codeship.com/running-node-js-linux-systemd/
### Ansible
https://docs.ansible.com/ansible

https://docs.ansible.com/ansible/latest/user_guide/intro_getting_started.html
